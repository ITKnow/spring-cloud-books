${AnsiColor.BRIGHT_GREEN}
   ___             __ _
  / __\___  _ __  / _(_) __ _
 / /  / _ \| '_ \| |_| |/ _` |
/ /__| (_) | | | |  _| | (_| |
\____/\___/|_| |_|_| |_|\__, |
                        |___/


${AnsiColor.BRIGHT_MAGENTA}::${AnsiColor.BRIGHT_WHITE} Spring Boot Version: ${AnsiColor.BRIGHT_YELLOW}${spring-boot.version}${spring-boot.formatted-version} ${AnsiColor.BRIGHT_MAGENTA}::
::${AnsiStyle.FAINT} https://gitee.com/darkranger/spring-cloud-books/tree/master/spring-cloud-config-server ${AnsiColor.BRIGHT_MAGENTA}::
${AnsiColor.BRIGHT_WHITE}