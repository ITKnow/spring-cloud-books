package com.wujunshen;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
@Slf4j
public class Config {
    public static void main(String[] args) {
        log.info("start execute Config....\n");
        SpringApplication.run(Config.class, args);
        log.info("end execute ConfigServerApplication....\n");
    }
}